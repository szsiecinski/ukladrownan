#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDialogButtonBox>
#include <armadillo>
#include <iostream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void About();
    void AboutQt();
    void PustyUklad();
    void RozwiazUklad();
    void PrzykladowyUklad();

private slots:
    void matrixSize();
    void checkVector();
    
private:
    Ui::MainWindow *ui;
    arma::mat getMatrix(QString text);
    arma::mat getVector(QString text);
    QString solutionToString(const arma::mat &vector);
};

#endif // MAINWINDOW_H
