#-------------------------------------------------
#
# Project created by QtCreator 2012-09-02T10:06:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UkladRownan
TEMPLATE = app

LIBS += -larmadillo

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
