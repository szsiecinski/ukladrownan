#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->actionO_programie, SIGNAL(triggered()), this, SLOT(About()));
    QObject::connect(ui->actionO_Qt, SIGNAL(triggered()), this, SLOT(AboutQt()));
    QObject::connect(ui->actionPusty_uklad, SIGNAL(triggered()), this, SLOT(PustyUklad()));
    QObject::connect(ui->actionPrzykladowy_uklad, SIGNAL(triggered()), this, SLOT(PrzykladowyUklad()));
    QObject::connect(ui->actionRozwiaz_uklad, SIGNAL(triggered()), this, SLOT(RozwiazUklad()));
    QObject::connect(ui->plainTextEdit, SIGNAL(textChanged()), this, SLOT(matrixSize()));
    //QObject::connect(ui->plainTextEdit_2, SIGNAL(textChanged()), this, SLOT(checkVector()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::AboutQt()
{
    QApplication::aboutQt();
}

void MainWindow::About()
{
    QMessageBox::about(this, QString::fromUtf8("O programie"), QString::fromUtf8("Program rozwiązujący układy równań w postaci macierzowej."));
}

void MainWindow::PustyUklad()
{
    ui->plainTextEdit->clear();
    ui->plainTextEdit_2->clear();
    ui->plainTextEdit_3->clear();
}

void MainWindow::PrzykladowyUklad()
{
    QString uklad = "3 5\n1 2";
    QString wyrazy_wolne = "12\n5";
    ui->plainTextEdit->setPlainText(uklad);
    ui->plainTextEdit_2->setPlainText(wyrazy_wolne);
}

void MainWindow::checkVector()
{
    /* liczba kolumn */
    int rows = ui->plainTextEdit_2->document()->lineCount();
    QStringList splitted = ui->plainTextEdit->toPlainText().split("\n");
    int cols = splitted[rows-1].split(" ").count();

    if(cols != 1)
    {
    /* komunikat do wyświetlenia na pasku statusu */
        ui->statusBar->showMessage(QString::fromUtf8("Niewłaściwa liczba kolumn"));
    }
    else
    {
        int rows1 = ui->plainTextEdit_2->document()->lineCount(); //liczba wierszy

        /* komunikat do wyświetlenia na pasku statusu */
        ui->statusBar->showMessage(QString::fromUtf8("Wierszy: ") + QString::number(rows1));
    }
}

void MainWindow::matrixSize()
{
    int rows1 = ui->plainTextEdit->document()->lineCount(); //liczba wierszy

    /* liczba kolumn */
    QStringList splitted = ui->plainTextEdit->toPlainText().split("\n");
    int cols1 = splitted[rows1-1].split(" ").count();

    /* komunikat do wyświetlenia na pasku statusu */
    ui->statusBar->showMessage(QString::fromUtf8("Wierszy: ") + QString::number(rows1)
                                  + QString::fromUtf8(", kolumn: ") + QString::number(cols1));
}

void MainWindow::RozwiazUklad()
{
    //funkcja rozwiazywania ukladu
    arma::mat uklad;
    arma::mat wyrazyWolne;
    arma::mat rozwiazanie;

    //int liczba_wierszy = 0;

    if(ui->plainTextEdit->document()->lineCount() == ui->plainTextEdit_2->document()->lineCount())
    {
        //liczba_wierszy = ui->plainTextEdit->document()->lineCount();
        // std::cout << liczba_wierszy << std::endl;
        uklad = getMatrix(ui->plainTextEdit->toPlainText());
        wyrazyWolne = getVector(ui->plainTextEdit_2->toPlainText());
        if(arma::det(uklad)!=0)
        {
            rozwiazanie = uklad.i() * wyrazyWolne;
            ui->plainTextEdit_3->setPlainText(solutionToString(rozwiazanie));
        }
        else
        {
            QMessageBox::warning(this, QString::fromUtf8("Błąd"), QString::fromUtf8("Nierozwiązywalny układ równań"), QDialogButtonBox::Ok, QDialogButtonBox::NoButton);
        }
    }
    else
    {
        QMessageBox::warning(this, QString::fromUtf8("Ostrzeżenie"), QString::fromUtf8("Liczba wierszy w obydwu polach różni się."), QDialogButtonBox::Ok, QDialogButtonBox::NoButton);
    }
}

arma::mat MainWindow::getMatrix(QString text)
{
    bool ok;
    //text.append("\n");
    QStringList wiersze = text.split("\n");
    QStringList liczby; //pomocnicza lista
    QStringList macierz[wiersze.count()]; //macierz tekstowa

    for(int i=0; i<wiersze.count(); i++)
    {
        liczby = wiersze[i].split(" ");
        macierz[i] = liczby;
    }

    //inicjalizacja macierzy
    arma::mat result(wiersze.count(), macierz[0].count());

    for(int i=0; i<wiersze.count(); i++)
    {
        for(int j=0; j<macierz[i].count(); j++)
        {
            result(i, j) = macierz[i].at(j).toDouble(&ok);
        }
    }

    return result;
}

arma::mat MainWindow::getVector(QString text)
{
    bool ok;
    QStringList wiersze = text.split("\n");
    arma::mat result(wiersze.count(), 1);

    for(int i=0; i<wiersze.count(); i++)
    {
        result(i,0)=wiersze[i].toDouble(&ok);
    }

    return result;
}

QString MainWindow::solutionToString(const arma::mat &vector)
{
    QString result;

    for(uint i=0; i<vector.n_rows; i++)
    {
        result+=QString("x");
        result+=QString::number(i+1);
        result+=QString(" = ");
        result+=QString::number(vector(i,0));
        result+='\n';
    }

    return result;
}
